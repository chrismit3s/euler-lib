# todo

* makefile
* testing suite

## types

* vector
  * both for double and int [and bigint?]
  * macro to create n dimensional vector
  * add, sub, dot prod
  * broadcasting
* bigint
  * kinda like python handles ints under the hood
  * int array with special addition, etc
  * SUBTRACTION

## datastructures

* list (generic?)
  * arraylist which automatically resizes
  * python indexing, append, etc
* set
  * hashset?
* bitfield
* [dict?]

## functions

* prime.h
  * get_primes (first n or up to n, as list)
  * is_prime (with array of primes to pass in)
* factor.h
  * get_factors (as list)
  * get_divisors (as list)
* math.h
  * int powmod(int base, int exp, int mod)
  * int_pow (opposite to int_log)
