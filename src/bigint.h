#ifndef _BIGINT_H
#define _BIGINT_H


#include <stdint.h>


#define BIGINT_DEBUG(x) (bigint_print_debug_named(#x, x))


typedef struct bigint {
	int32_t size;
	uint32_t* digits;
} bigint;


/* get a bigint represeting zero
 */
bigint* bigint_zero(void);

/* various constructors
 */
bigint* bigint_from_int32(int32_t a);  // signed int --> bigint
bigint* bigint_from_uint32(uint32_t a);  // unsigned int --> bigint
bigint* bigint_from_int64(int64_t a);  // signed long long --> bigint
bigint* bigint_from_uint64(uint64_t a);  // unsigned long long --> bigint

/* frees a bigint
 */
void bigint_free(bigint* x);

/* get a copy of a bigint in a new variable
 */
bigint* bigint_copy(bigint* orig);

/* copies a bigint to another one, overwriting its previous value
 */
bigint* bigint_copy_to(bigint* dest, bigint* src);

/* adds a to b, and returns their sum as a new bigint; takes their
 * signs into account; returns null if theres not enough memory, and
 * using a and b afterwards is undefined behavior, they should be freed
 * and nulled, not to be used again
 */
bigint* bigint_add(bigint* a, bigint* b);

/* increments a by b, the result is stored in a; takes their signs into
 * account; returns null if theres not enough memory, and using a and b
 * afterwards is undefined behavior, they should be freed and nulled, not
 * to be used again
 */
bigint* bigint_add_to(bigint* a, bigint* b);

/* increments a by b, the result is stored in a; does not check their
 * signs, only adds them digit wise; returns null if theres not enough
 * memory, and using a and b afterwards is undefined behavior, they
 * should be freed and nulled, not to be used again
 */
bigint* bigint_add_to_digitwise(bigint* a, bigint* b);

/* returns > 0 if a > b; returns < 0 if a < b; returns 0 if a == b;
 * just like a comparator in java
 */
int bigint_compare(bigint* a, bigint* b);

/* print a bigint as a stuct (useful for debugging)
 */
void bigint_print_debug(bigint* x);

/* prints a named bigint variable; not to be called by hand, use the
 * BIGINT_DEBUG (defined in bigint.h) macro or bigint_print_debug for
 * that
 */
void bigint_print_debug_named(const char* name, bigint* x);

/* get a string represenation of x in a specific base (or in base 10)
 */
char* bigint_as_str(bigint* x);
char* bigint_as_str_in_base(bigint* x, uint32_t base);


#endif /* _BIGINT_H */
