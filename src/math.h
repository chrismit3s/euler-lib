#ifndef _MATH_H
#define _MATH_H


#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define CLAMP(x, a, b) (MAX((a), MIN((x), (b))))

#define SIGN(a) ((a) == (typeof(a))0 ? (a) : (a > (typeof(a))0 ? (typeof(a))1 : (typeof(a))-1))
#define ABS(a) ((a) < (typeof(a))0 ? -(a) : (a))


/* computes the greatest common divisor
 */
uint32_t gcd(uint32_t a, uint32_t b);

/* computes the lowest common multiple
 */
uint32_t lcm(uint32_t a, uint32_t b);

/* computes n!
 */
uint32_t fac(uint32_t n);

/* computes the binomial coefficient (or "i choose j")
 */
uint32_t bin(uint32_t i, uint32_t j);

/* computes the integer log base b of x so that
 * log_b(x) < int_log(x, b) <= log_b(x) + 1; where
 * log_b(x) denotes the (real-valued) logarithm of
 * x to the base b
 */
uint32_t int_log(uint32_t x, uint32_t b);


#endif /* _MATH_H */
