#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "math.h"


/* computes the greatest common divisor
 */
uint32_t gcd(uint32_t a, uint32_t b) {
	return (a == 0) ? b : gcd(b % a, a);
}

/* computes the lowest common multiple
 */
uint32_t lcm(uint32_t a, uint32_t b) {
	return a * b / gcd(a, b);
}

/* computes n!
 */
uint32_t fac(uint32_t n) {
	uint32_t res = 1;
	for (int i = 2; i <= n; ++i)
		res *= i;
	return res;
}

/* computes the binomial coefficient (or "i choose j")
 */
uint32_t bin(uint32_t i, uint32_t j) {
	if (i < j)
		return 0;
	return fac(i) / fac(j) / fac(i - j);
}

/* computes the integer log base b of x so that
 * log_b(x) < int_log(x, b) <= log_b(x) + 1; where
 * log_b(x) denotes the (real-valued) logarithm of
 * x to the base b
 */
uint32_t int_log(uint32_t x, uint32_t b) {
	uint32_t res = 0;
	while (x != 0) {
		res++;
		x /= b;
	}
	return res;
}
