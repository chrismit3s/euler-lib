#include <stdio.h>
#include <stdlib.h>
#include "bigint.h"


int main(void) {
	bigint* zero = bigint_zero();
	bigint* one_i = bigint_from_int32(1);
	bigint* one_l = bigint_from_int64(1L);
	bigint* two = bigint_from_int32(2);
	bigint* three = bigint_from_uint64(3);
	bigint* neg_three = bigint_from_int64(-3L);

	int64_t big_value = 0xFFFFFFFFFFF;
	bigint* big = bigint_from_uint64(big_value);

	int ret;
	if ((ret = bigint_compare(one_i, one_l)) != 0) {
		printf("long and int constructors are not equal [compare returned %d]\n", ret);
		BIGINT_DEBUG(one_i);
		BIGINT_DEBUG(one_l);
	}
	bigint* one = one_i;

	if ((ret = bigint_compare(two, one)) <= 0) {
		printf("two smaller or equal to one [compare returned %d]\n", ret);
		BIGINT_DEBUG(two);
		BIGINT_DEBUG(one);
	}

	if ((ret = bigint_compare(two, zero)) <= 0) {
		printf("two smaller or equal to zero [compare returned %d]\n", ret);
		BIGINT_DEBUG(two);
		BIGINT_DEBUG(zero);
	}

	if ((ret = bigint_compare(neg_three, zero)) >= 0) {
		printf("negative three larger or equal to zero [compare returned %d]\n", ret);
		BIGINT_DEBUG(neg_three);
		BIGINT_DEBUG(zero);
	}

	printf("%lld\n", big_value);
	BIGINT_DEBUG(big);
	
	return EXIT_SUCCESS;
}

