#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "bigint.h"
#include "math.h"


#define BIGINT_DIGIT_BASE (UINT32_MAX / 2 + 1)
#define BIGINT_SHIFT (31)
#define BIGINT_MASK ((uint32_t)(0x7FFFFFFFFFFFFFFF))


/* get a bigint represeting zero
 */
bigint* bigint_zero(void) {
	bigint* x = (bigint*)malloc(sizeof(bigint));

	x->size = 0;
	x->digits = NULL;

	return x;
}

/* signed int --> bigint
 */
bigint* bigint_from_int32(int32_t a) {
	bigint* x = bigint_zero();

	// early return
	if (a == 0)
		return x;

	// at least on digit + floor division (size sign is sign of number)
	x->size = a / BIGINT_DIGIT_BASE + SIGN(a);
	x->digits = (uint32_t*)malloc(sizeof(uint32_t) * ABS(x->size));

	// initialize each digit
	int32_t i = 0;
	a = ABS(a);
	while (a != 0) {
		x->digits[i++] = a % BIGINT_DIGIT_BASE;
		a /= BIGINT_DIGIT_BASE;
	}

	return x;
}

/* unsigned int --> bigint
 */
bigint* bigint_from_uint32(uint32_t a) {
	bigint* x = bigint_zero();

	// early return
	if (a == 0U)
		return x;

	// at least on digit + floor division (size sign is sign of number)
	x->size = a / BIGINT_DIGIT_BASE + SIGN(a);
	x->digits = (uint32_t*)malloc(sizeof(uint32_t) * ABS(x->size));

	// initialize each digit
	int32_t i = 0;
	a = ABS(a);
	while (a != 0U) {
		x->digits[i++] = a % BIGINT_DIGIT_BASE;
		a /= BIGINT_DIGIT_BASE;
	}

	return x;
}

/* signed long long --> bigint
 */
bigint* bigint_from_int64(int64_t a) {
	bigint* x = bigint_zero();

	// early return
	if (a == 0L)
		return x;

	// get number of digits and malloc space
	x->size = int_log(ABS(a), BIGINT_DIGIT_BASE) * SIGN(a);
	x->digits = (uint32_t*)malloc(sizeof(uint32_t) * ABS(x->size));

	// initialize each digit
	int32_t i = 0;
	a = ABS(a);
	while (a != 0L) {
		x->digits[i++] = a & BIGINT_MASK;
		a >>= BIGINT_SHIFT;
	}

	return x;
}

/* unsigned long long --> bigint
 */
bigint* bigint_from_uint64(uint64_t a) {
	bigint* x = bigint_zero();

	// early return
	if (a == 0UL)
		return x;

	// get number of digits and malloc space
	x->size = int_log(ABS(a), BIGINT_DIGIT_BASE) * SIGN(a);
	x->digits = (uint32_t*)malloc(sizeof(uint32_t) * ABS(x->size));

	// initialize each digit
	int32_t i = 0;
	a = ABS(a);
	while (a != 0UL) {
		x->digits[i++] = a % BIGINT_DIGIT_BASE;
		a /= BIGINT_DIGIT_BASE;
	}

	return x;
}

/* frees a bigint
 */
void bigint_free(bigint* x) {
	free(x->digits);
	x->digits = NULL;
	free(x);
}

/* get a copy of a bigint in a new variable
 */
bigint* bigint_copy(bigint* orig) {
	bigint* copy = bigint_zero();
	return bigint_copy_to(copy, orig);
}

/* copies a bigint to another one, overwriting its previous value
 */
bigint* bigint_copy_to(bigint* dest, bigint* src) {
	dest->size = src->size;
	dest->digits = (digit*)malloc(sizeof(digit) * ABS(dest->size));

	for (int32_t i = 0; i != ABS(dest->size); ++i)
		dest->digits[i] = src->digits[i];

	return dest;
}

/* adds a to b, and returns their sum as a new bigint; takes their
 * signs into account; returns null if theres not enough memory, and
 * using a and b afterwards is undefined behavior, they should be freed
 * and nulled, not to be used again
 */
bigint* bigint_add(bigint* a, bigint* b) {
	// avoid a second malloc call
	if (ABS(a->size) > ABS(b->size))
		return bigint_add_to(bigint_copy(a), b);
	else
		return bigint_add_to(bigint_copy(b), a);
}

/* increments a by b, the result is stored in a; takes their signs into
 * account; returns null if theres not enough memory, and using a and b
 * afterwards is undefined behavior, they should be freed and nulled, not
 * to be used again
 */
bigint* bigint_add_to(bigint* a, bigint* b) {
	if (SIGN(a->size) * SIGN(b->size) >= 0)  // same sign or at least one zero
		return bigint_add_to_digitwise(a, b);
	else
		return bigint_sub_from_digitwise(a, b);
}

/* increments a by b, the result is stored in a; does not check their
 * signs, only adds them digit wise; returns null if theres not enough
 * memory, and using a and b afterwards is undefined behavior, they
 * should be freed and nulled, not to be used again
 */
bigint* bigint_add_to_digitwise(bigint* a, bigint* b) {
	int32_t min_size = MIN(ABS(a->size), ABS(b->size));
	int32_t max_size = MAX(ABS(a->size), ABS(b->size));

	// add first digits where both numbers arent empty
	uint32_t carry = 0U;
	for (int32_t i = 0; i != min_size; ++i) {
		carry += a->digits[i] + b->digits[i];
		a->digits[i] = carry & BIGINT_MASK;
		carry >>= BIGINT_SHIFT;
	}

	// take care of the remaining digits
	if (ABS(a->size) == max_size) {  // a is the longer number
		// add remaining digits
		for (int32_t i = min_size; i != max_size; ++i) {
			carry += a->digits[i];
			a->digits[i] = carry & BIGINT_MASK;
			carry >>= BIGINT_SHIFT;
		}
	}
	else {  // b is the longer number
		// realloc a and check for out-of-memory NULL
		bigint* temp;
		if ((temp = (bigint*)realloc(a, ABS(b->size))) == NULL)
			return NULL;
		a = temp;
		a->size = b->size;

		// add remaining digits
		for (int32_t i = min_size; i != max_size; ++i) {
			carry += b->digits[i];
			a->digits[i] = carry & BIGINT_MASK;
			carry >>= BIGINT_SHIFT;
		}
	}

	// check last carry bit
	if (carry != 0) {
		// realloc a and check for out-of-memory NULL
		bigint* temp;
		if ((temp = (bigint*)realloc(a, ABS(a->size) + 1)) == NULL)
			return NULL;
		a = temp;

		// increase size by one
		a->digits[max_size] = carry;
		a->size += SIGN(a->size);
	}

	return a;
}

/* subtracts b from a, and returns their difference as a new bigint; takes
 * their signs into account; returns null if theres not enough memory, and
 * using a and b afterwards is undefined behavior, they should be freed
 * and nulled, not to be used again
 */
bigint* bigint_sub(bigint* a, bigint* b) {
	return bigint_sub_from(bigint_copy(a), b);
}

/* decrements a by b, the result is stored in a; takes their signs into
 * account; returns null if theres not enough memory, and using a and b
 * afterwards is undefined behavior, they should be freed and nulled, not
 * to be used again
 */
bigint* bigint_sub_from(bigint* a, bigint* b) {
	if (SIGN(a->size) * SIGN(b->size) >= 0)  // same sign or at least one zero
		return bigint_sub_from_digitwise(a, b);
	else
		return bigint_add_to_digitwise(a, b);
}

/* decrements a by b, the result is stored in a; does not check their
 * signs, only subtracts them digit wise; returns null if theres not enough
 * memory, and using a and b afterwards is undefined behavior, they
 * should be freed and nulled, not to be used again
 */
bigint* bigint_sub_from_digitwise(bigint* a, bigint* b) {
	int32_t min_size = MIN(ABS(a->size), ABS(b->size));
	int32_t max_size = MAX(ABS(a->size), ABS(b->size));

	// sub first digits where both numbers arent empty
	uint32_t borrow = 0U, subtotal = 0U;
	for (int32_t i = 0; i != min_size; ++i) {
		borrow = (1 << BIGINT_SHIFT) + a->digits[i] - b->digits[i] - borrow;
		a->digits[i] = borrow & BIGINT_MASK;
		borrow >>= BIGINT_SHIFT;
	}

	// take care of the remaining digits
	if (ABS(a->size) == max_size) {  // a is the longer number
		// add remaining digits
		for (int32_t i = min_size; i != max_size; ++i) {
			borrow = (1 << BIGINT_SHIFT) + a->digits[i] - borrow;
			a->digits[i] = borrow & BIGINT_MASK;
			borrow >>= BIGINT_SHIFT;
		}
	}
	else {  // b is the longer number
		// realloc a and check for out-of-memory NULL
		bigint* temp;
		if ((temp = (bigint*)realloc(a, ABS(b->size))) == NULL)
			return NULL;
		a = temp;
		a->size = b->size;

		// sub remaining digits
		for (int32_t i = min_size; i != max_size; ++i) {
			borrow = (1 << BIGINT_SHIFT) - b->digits[i] - borrow;
			a->digits[i] = borrow & BIGINT_MASK;
			borrow >>= BIGINT_SHIFT;
		}
	}


	// check last borrow bit
	if (borrow != 0) {
		// realloc a and check for out-of-memory NULL
		bigint* temp;
		if ((temp = (bigint*)realloc(a, ABS(a->size) + 1)) == NULL)
			return NULL;
		a = temp;
		a->size += SIGN(a->size);
	}

	return a;
}

/* returns > 0 if a > b; returns < 0 if a < b; returns 0 if a == b;
 * just like a comparator in java
 */
int bigint_compare(bigint* a, bigint* b) {
	if (a->size != b->size)
		return a->size - b->size;

	for (int32_t i = ABS(a->size) - 1; i >= 0; --i)
		if (a->digits[i] != b->digits[i])
			return (int)(a->digits[i] - b->digits[i]);

	return 0;
}

void bigint_print_debug(bigint* x) {
	bigint_print_debug_named("bigint", x);
}

/* prints a named bigint variable; not to be called by hand, use the
 * BIGINT_DEBUG (defined in bigint.h) macro for that
 */
void bigint_print_debug_named(const char* name, bigint* x) {
	printf("%s = %s {\n", name, bigint_as_str(x));
	printf("\tsize = %d\n", x->size);

	printf("\tdigits = [");
	int i;
	for (i = 0; i != ABS(x->size) - 1; ++i)
		printf("%d, ", x->digits[i]);
	printf("%d]\n", x->digits[i]);

	printf("}\n");
}

/* get a string represenation of x in a specific base (or in base 10)
 */
char* bigint_as_str(bigint* x) {
	return bigint_as_str_in_base(x, 10);
}
char* bigint_as_str_in_base(bigint* x, uint32_t base) {
	// TODO
	return "<some-val>";
}
